# Prefect Huawei Cloud Block

## Welcome!
The prefect-hwc collection enables you to use HUAWEI CLOUD service capabilities in Prefect more efficiently.
## Getting Started

```bash
pip install -U "prefect"

pip install prefect-hwc

prefect block register --module prefect-hwc
```

## Contributing

Thank you for wanting to contribute, you can contribute to [this code repository](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-prefect-block-python) .If you have any questions, consult the community.