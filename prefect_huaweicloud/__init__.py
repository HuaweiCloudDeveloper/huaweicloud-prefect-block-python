# ensure core blocks are registered

from prefect_huaweicloud.obs_block import ObsBlock
from prefect_huaweicloud.dew_block import DewBlock
from prefect_huaweicloud.dew_client import DewClient
__all__ = ["ObsBlock","DewBlock","DewClient"]

