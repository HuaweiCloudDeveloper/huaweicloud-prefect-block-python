import sys
from pathlib import Path
sys.path.append(str(Path(__file__).resolve().parents[1]))
from dew_block import DewBlock
from prefect import flow

KEY_ID='key_id'
KEY_ALIAS='key_alias'
GRANT_ID='grant_id'
KEYPAIR_NAME='keypair_name'
KEY='key'
TYPE='type'
ID='id'
SECRET_NAME='secret_name'
SECRET_ID='secret_id'
STAGE_NAME='stage_name'
STAGE_ID='stage_id'
EVENT_NAME='event_name'
def kms_parameters():
     test_key = None
     key_alias = None
     if "key_alias" in huaweicloud_dew_block.dew_parameters:
          key_alias = huaweicloud_dew_block.dew_parameters["key_alias"]
          test_key = huaweicloud_dew_block.create_key(DewBlock.CreatekeyOptions( key_alias = key_alias))
     return dict(
          key_alias = key_alias if key_alias is not None else None, 
          key_id = test_key.key_info.key_id if test_key is not None else None, 
          key_description = None, 
          pending_days = huaweicloud_dew_block.dew_parameters["pending_days"] if "pending_days" in huaweicloud_dew_block.dew_parameters else None, 

          random_data_length = huaweicloud_dew_block.dew_parameters["random_data_length"] if "random_data_length" in huaweicloud_dew_block.dew_parameters else None, 
          datakey_length = huaweicloud_dew_block.dew_parameters["datakey_length"] if "datakey_length" in huaweicloud_dew_block.dew_parameters else None, 
          datakey_plain_length = huaweicloud_dew_block.dew_parameters["datakey_plain_length"] if "datakey_plain_length" in huaweicloud_dew_block.dew_parameters else None, 
          datakey_cipher_length = huaweicloud_dew_block.dew_parameters["datakey_cipher_length"] if "datakey_cipher_length" in huaweicloud_dew_block.dew_parameters else None, 
          cipher_text = None, 
          plain_text = None, 
          datakey_dgst = None, 

          asymmetric_key_id = huaweicloud_dew_block.dew_parameters["asymmetric_key_id"] if "asymmetric_key_id" in huaweicloud_dew_block.dew_parameters else None, 

          grantee_principal = huaweicloud_dew_block.dew_parameters["grantee_principal"] if "grantee_principal" in huaweicloud_dew_block.dew_parameters else None, 
          listOperationsbody = huaweicloud_dew_block.dew_parameters["listOperationsbody"] if "listOperationsbody" in huaweicloud_dew_block.dew_parameters else None, 
          grant_id = None, 

          rotation_interval = huaweicloud_dew_block.dew_parameters["rotation_interval"] if "rotation_interval" in huaweicloud_dew_block.dew_parameters else None, 

          signing_algorithm = huaweicloud_dew_block.dew_parameters["signing_algorithm"] if "signing_algorithm" in huaweicloud_dew_block.dew_parameters else None, 
          message = huaweicloud_dew_block.dew_parameters["message"] if "message" in huaweicloud_dew_block.dew_parameters else None, 
          message_type = huaweicloud_dew_block.dew_parameters["message_type"] if "message_type" in huaweicloud_dew_block.dew_parameters else None, 
          signature = None, 

          action = huaweicloud_dew_block.dew_parameters["action"] if "action" in huaweicloud_dew_block.dew_parameters else None, 
          key = huaweicloud_dew_block.dew_parameters[KEY] if KEY in huaweicloud_dew_block.dew_parameters else None, 
          version_id = huaweicloud_dew_block.dew_parameters["version_id"] if "version_id" in huaweicloud_dew_block.dew_parameters else None, 
     )


def kps_parameters():
     key_pair_list = huaweicloud_dew_block.list_keypairs()
     test_key = None
     name = None
     if "name" in huaweicloud_dew_block.dew_parameters:
          name = huaweicloud_dew_block.dew_parameters.get("name")
          if name in list(x.keypair.name for x in key_pair_list.keypairs[:]):
               huaweicloud_dew_block.delete_keypair(keypair_name = name)
          test_key = huaweicloud_dew_block.create_keypair(DewBlock.CreateKeypairOptions(name = name))
     return dict(
          name = name, 
          keypair_name = name, 
          private_key = test_key.keypair.private_key, 
          description = None, 
          id = huaweicloud_dew_block.dew_parameters["id"] if "id" in huaweicloud_dew_block.dew_parameters else None, 
          kms_key_name = huaweicloud_dew_block.dew_parameters["kms_key_name"] if "kms_key_name" in huaweicloud_dew_block.dew_parameters else None, 
          type = huaweicloud_dew_block.dew_parameters["type"] if "type" in huaweicloud_dew_block.dew_parameters else None, 
          key = huaweicloud_dew_block.dew_parameters[KEY] if KEY in huaweicloud_dew_block.dew_parameters else None, 
          task_id = None, 
          disable_password = huaweicloud_dew_block.dew_parameters["disable_password"] if "disable_password" in huaweicloud_dew_block.dew_parameters else None, 
     )


def csms_parameters():
     secret_list = huaweicloud_dew_block.list_secrets()
     secret_string = None
     event_name = None
     test_secret = None
     if "name" and "secret_string" in huaweicloud_dew_block.dew_parameters:
          name = huaweicloud_dew_block.dew_parameters.get("name")
          secret_string = huaweicloud_dew_block.dew_parameters["secret_string"]

          if name in list(x.name for x in secret_list.secrets[:]):
               huaweicloud_dew_block.delete_secret(secret_name = name)
          test_secret = huaweicloud_dew_block.create_secret(DewBlock.CreateSecretOptions(name = name, secret_string = secret_string))

     secret_event = huaweicloud_dew_block.list_secret_events()

     if event_name in list(x.name for x in secret_event.events[:]):
          huaweicloud_dew_block.delete_secret_event(event_name = event_name)
     return dict(
          secret_name = test_secret.secret.name, 
          version_id = huaweicloud_dew_block.dew_parameters["version_id"] if "version_id" in huaweicloud_dew_block.dew_parameters else None, 
          secret_blob = None, 
          expire_time = huaweicloud_dew_block.dew_parameters["expire_time"] if "expire_time" in huaweicloud_dew_block.dew_parameters else None, 
          secret_string = secret_string, 
          stage_name = huaweicloud_dew_block.dew_parameters[STAGE_NAME] if STAGE_NAME in huaweicloud_dew_block.dew_parameters else None, 
          update_stage_name = huaweicloud_dew_block.dew_parameters["update_stage_name"] if "update_stage_name" in huaweicloud_dew_block.dew_parameters else None, 
          update_version_id = huaweicloud_dew_block.dew_parameters["update_version_id"] if "update_version_id" in huaweicloud_dew_block.dew_parameters else None, 
          secret_id = test_secret.secret.id, 
          key = huaweicloud_dew_block.dew_parameters[KEY] if KEY in huaweicloud_dew_block.dew_parameters else None, 
          action = huaweicloud_dew_block.dew_parameters["action0"] if "action0" in huaweicloud_dew_block.dew_parameters else None, 
          action1 = huaweicloud_dew_block.dew_parameters["action1"] if "action1" in huaweicloud_dew_block.dew_parameters else None, 

          value = huaweicloud_dew_block.dew_parameters["value"] if "value" in huaweicloud_dew_block.dew_parameters else None, 
          values = huaweicloud_dew_block.dew_parameters["values"] if "values" in huaweicloud_dew_block.dew_parameters else None, 
          resource_instances = huaweicloud_dew_block.dew_parameters["resource_instances"] if "resource_instances" in huaweicloud_dew_block.dew_parameters else None, 
          event_name = huaweicloud_dew_block.dew_parameters[EVENT_NAME] if EVENT_NAME in huaweicloud_dew_block.dew_parameters else None, 
          event_types = huaweicloud_dew_block.dew_parameters["event_types"] if "event_types" in huaweicloud_dew_block.dew_parameters else None, 
          state = huaweicloud_dew_block.dew_parameters["state"] if "state" in huaweicloud_dew_block.dew_parameters else None, 
          target_type = huaweicloud_dew_block.dew_parameters["target_type"] if "target_type" in huaweicloud_dew_block.dew_parameters else None, 
          target_id = huaweicloud_dew_block.dew_parameters["target_id"] if "target_id" in huaweicloud_dew_block.dew_parameters else None, 
          target_name = huaweicloud_dew_block.dew_parameters["target_name"] if "target_name" in huaweicloud_dew_block.dew_parameters else None, 
          recovery_window_in_days = huaweicloud_dew_block.dew_parameters["recovery_window_in_days"] if "recovery_window_in_days" in huaweicloud_dew_block.dew_parameters else None, 

     )


@flow(name = "test_key_lifecycle_management")
def test_key_lifecycle_management(kms_params):
     row_key_alias = kms_params.get(KEY_ALIAS)
     huaweicloud_dew_block.create_key(DewBlock.CreatekeyOptions(key_alias = kms_params.get(KEY_ALIAS)))
     huaweicloud_dew_block.disable_key(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.enable_key(key_id = kms_params.get(KEY_ID))
     kms_params[KEY_ALIAS] = "test_alias"
     huaweicloud_dew_block.update_key_alias(key_id = kms_params.get(KEY_ID), key_alias = kms_params.get(KEY_ALIAS))
     kms_params[KEY_ALIAS] = row_key_alias
     huaweicloud_dew_block.update_key_alias(key_id = kms_params.get(KEY_ID), key_alias = kms_params.get(KEY_ALIAS))
     kms_params['key_description'] = "test_update_description"
     huaweicloud_dew_block.update_key_description(key_id = kms_params.get(KEY_ID), key_description = kms_params.get('key_description'))
     huaweicloud_dew_block.delete_key(key_id = kms_params.get(KEY_ID), pending_days = kms_params.get('pending_days'))
     huaweicloud_dew_block.cancel_key_deletion(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.enable_key(key_id = kms_params.get(KEY_ID))


@flow(name = 'test_data_key_management')
def test_data_key_management(kms_params):
     huaweicloud_dew_block.create_data_key_without_plaintext(key_id = kms_params.get(KEY_ID))
     create_data_key_response = huaweicloud_dew_block.create_data_key(key_id = kms_params.get(KEY_ID), datakey_length = kms_params.get('datakey_length'))
     huaweicloud_dew_block.create_random(random_data_length = kms_params.get('random_data_length'))
     kms_params["cipher_text"] = create_data_key_response.cipher_text
     decrypt_data_key_response = huaweicloud_dew_block.decrypt_data_key(key_id = kms_params.get(KEY_ID), cipher_text = kms_params.get('cipher_text'), datakey_cipher_length = kms_params.get('datakey_cipher_length'))
     kms_params["datakey_dgst"] = decrypt_data_key_response.datakey_dgst
     kms_params["plain_text"] = decrypt_data_key_response.data_key+decrypt_data_key_response.datakey_dgst
     huaweicloud_dew_block.encrypt_data_key(key_id = kms_params.get(KEY_ID), plain_text = kms_params.get('plain_text'), datakey_plain_length = kms_params.get('datakey_plain_length'))


@flow(name = 'test_key_grant_management')
def test_key_grant_management(kms_params):
     create_grant_response = huaweicloud_dew_block.create_grant(DewBlock.CreateGrantOptions(key_id = kms_params.get(KEY_ID), 
                         grantee_principal = kms_params.get("grantee_principal"), listOperationsbody = kms_params.get("listOperationsbody")))
     kms_params[GRANT_ID] = create_grant_response.grant_id
     huaweicloud_dew_block.list_retirable_grants()
     huaweicloud_dew_block.cancel_self_grant(key_id = kms_params.get(KEY_ID), grant_id = kms_params.get(GRANT_ID))
     huaweicloud_dew_block.create_grant(DewBlock.CreateGrantOptions(key_id = kms_params.get(KEY_ID), 
                         grantee_principal = kms_params.get("grantee_principal"), listOperationsbody = kms_params.get("listOperationsbody")))
     huaweicloud_dew_block.cancel_grant(key_id = kms_params.get(KEY_ID), grant_id = kms_params.get(GRANT_ID))
     huaweicloud_dew_block.list_grants(key_id = kms_params.get(KEY_ID))


@flow(name = 'test_key_pair_management')
def test_key_pair_management(kps_params):
     huaweicloud_dew_block.import_private_key(name = kps_params.get('name'), type = kps_params.get(TYPE), kms_key_name = kps_params.get('kms_key_name'), private_key = kps_params.get('private_key'))
     huaweicloud_dew_block.export_private_key(name = kps_params.get('name'))
     huaweicloud_dew_block.list_keypair_detail(keypair_name = kps_params.get(KEYPAIR_NAME))
     huaweicloud_dew_block.list_keypairs()
     huaweicloud_dew_block.clear_private_key(keypair_name = kps_params.get(KEYPAIR_NAME))

     kps_params['description'] = "test_update_description"
     huaweicloud_dew_block.update_keypair_description(keypair_name = kps_params.get(KEYPAIR_NAME), description = kps_params.get('description'))

     huaweicloud_dew_block.delete_keypair(keypair_name = kps_params.get(KEYPAIR_NAME))


@flow(name = 'test_key_pair_task_management')
def test_key_pair_task_management(kps_params):
     kps_params["task_id"] = huaweicloud_dew_block.associate_keypair(DewBlock.AssociateKeypairOptions(keypair_name = kps_params.get(KEYPAIR_NAME), 
                         id = kps_params.get(ID), type = kps_params.get(TYPE), key = kps_params.get(KEY), disable_password = kps_params.get('disable_password'))).task_id
     huaweicloud_dew_block.batch_associate_keypair(DewBlock.BatchAssociateKeypairOptions(keypair_name = kps_params.get(KEYPAIR_NAME), 
                        id = kps_params.get(ID), type = kps_params.get(TYPE), key = kps_params.get(KEY), disable_password = kps_params.get('disable_password')))
     huaweicloud_dew_block.delete_all_failed_task()
     huaweicloud_dew_block.disassociate_keypair(id = kps_params.get(ID), type = kps_params.get(TYPE), key = kps_params.get(KEY))
     huaweicloud_dew_block.list_failed_task()
     huaweicloud_dew_block.list_keypair_task(task_id = kps_params.get('task_id'))
     huaweicloud_dew_block.list_running_task()


@flow(name = 'test_small_data_encryption')
def test_small_data_encryption(kms_params):
     encrypt_data_response = huaweicloud_dew_block.encrypt_data(key_id = kms_params.get(KEY_ID), plain_text = kms_params.get("plain_text"), encryption_algorithm = kms_params.get("encryption_algorithm"))
     kms_params["cipher_text"] = encrypt_data_response.cipher_text
     huaweicloud_dew_block.decrypt_data(cipher_text = kms_params.get("cipher_text"), key_id = kms_params.get(KEY_ID), encryption_algorithm = kms_params.get("encryption_algorithm"))


@flow(name = 'test_list_key')
def test_list_key(kms_params):
     huaweicloud_dew_block.list_keys(DewBlock.ListKeysOptions())
     huaweicloud_dew_block.list_key_detail(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.show_public_key(asymmetric_key_id = kms_params.get("asymmetric_key_id"))
     huaweicloud_dew_block.show_user_instances()
     huaweicloud_dew_block.show_user_quotas()


@flow(name = 'test_key_rotation_management')
def test_key_rotation_management(kms_params):
     huaweicloud_dew_block.enable_key_rotation(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.show_key_rotation_status(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.update_key_rotation_interval(key_id = kms_params.get(KEY_ID), rotation_interval = kms_params.get("rotation_interval"))
     huaweicloud_dew_block.disable_key_rotation(key_id = kms_params.get(KEY_ID))


@flow(name = 'test_signature_verification')
def test_signature_verification(kms_params):
     row_sign = huaweicloud_dew_block.sign(kms_params.get("asymmetric_key_id"), kms_params.get("message"), 
                                 kms_params.get("signing_algorithm"), kms_params.get("message_type"))
     kms_params['signature'] = row_sign.signature
     huaweicloud_dew_block.validate_signature(options = DewBlock.ValidateSignatureOptions(asymmetric_key_id = kms_params.get("asymmetric_key_id"), 
                         signature = kms_params.get("signature"), message = kms_params.get("message"), signing_algorithm = kms_params.get("signing_algorithm"), message_type = kms_params.get("message_type")))


@flow(name = 'test_lifecycle_management')
def test_lifecycle_management(csms_params):
     huaweicloud_dew_block.show_secret(secret_name = csms_params.get(SECRET_NAME))
     csms_params["secret_blob"] = huaweicloud_dew_block.download_secret_blob(secret_name = csms_params.get(SECRET_NAME)).secret_blob
     huaweicloud_dew_block.update_secret(DewBlock.UpdateSecretOptions(secret_name = csms_params.get(SECRET_NAME), ))
     huaweicloud_dew_block.delete_secret_for_schedule(secret_name = csms_params.get(SECRET_NAME), 
                                                      recovery_window_in_days = csms_params.get("recovery_window_in_days"))
     huaweicloud_dew_block.restore_secret(secret_name = csms_params.get(SECRET_NAME))
     huaweicloud_dew_block.delete_secret(secret_name = csms_params.get(SECRET_NAME))
     huaweicloud_dew_block.upload_secret_blob(secret_blob = csms_params.get("secret_blob"))


@flow(name = 'test_credential_version_management')
def test_credential_version_management(csms_params):
     huaweicloud_dew_block.create_secret_version(secret_name = csms_params.get(SECRET_NAME), secret_string = csms_params.get("secret_string"))
     huaweicloud_dew_block.show_secret_version(secret_name = csms_params.get(SECRET_NAME), version_id = csms_params.get("version_id"))
     huaweicloud_dew_block.list_secret_versions(secret_name = csms_params.get(SECRET_NAME))
     huaweicloud_dew_block.update_version(secret_name = csms_params.get(SECRET_NAME), version_id = csms_params.get("version_id"), expire_time = csms_params.get("expire_time"))


@flow(name = 'test_credential_version_status_management')
def test_credential_version_status_management(csms_params):
     show_secret_stage_response = huaweicloud_dew_block.show_secret_stage(secret_name = csms_params.get(SECRET_NAME), 
                                                                          stage_name = csms_params.get(STAGE_NAME))
     csms_params[STAGE_NAME] = csms_params["update_stage_name"]
     csms_params["version_id"] = show_secret_stage_response.stage.version_id
     huaweicloud_dew_block.update_secret_stage(secret_name = csms_params.get(SECRET_NAME), 
                                               stage_name = csms_params.get(STAGE_NAME), 
                                               version_id = csms_params.get("version_id"))
     huaweicloud_dew_block.delete_secret_stage(secret_name = csms_params.get(SECRET_NAME), 
                                               stage_name = csms_params.get(STAGE_NAME))


@flow(name = 'test_voucher_label_management')
def test_voucher_label_management(csms_params):
    huaweicloud_dew_block.batch_create_or_delete_tags(secret_id = csms_params.getSECRET_ID, 
                                                      key = csms_params.get(KEY), action = csms_params.get("action"))
    huaweicloud_dew_block.create_secret_tag(secret_id = csms_params.getSECRET_ID, key = csms_params.get(KEY))
    huaweicloud_dew_block.list_project_secrets_tags()
    huaweicloud_dew_block.list_resource_instances(DewBlock.ListResourceInstancesOptions(
         action = csms_params.get("action1"), resource_instances = csms_params.get("resource_instances")))
    huaweicloud_dew_block.list_secret_tags(secret_id = csms_params.getSECRET_ID)
    huaweicloud_dew_block.delete_secret_tag(secret_id = csms_params.getSECRET_ID, key = csms_params.get(KEY))


@flow(name = 'test_event_management')
def test_event_management(csms_params):
     huaweicloud_dew_block.create_secret_event(DewBlock.CreateSecretEventOptions(target_type = csms_params.get("target_type"), 
                                                                                 target_id = csms_params.get("target_id"), 
                                                                                 target_name = csms_params.get("target_name"), 
                                                                                 event_name = csms_params.get(EVENT_NAME), 
                                                                                 event_types = csms_params.get("event_types"), 
                                                                                 state = csms_params.get("state")))
     huaweicloud_dew_block.show_secret_event(event_name = csms_params.get(EVENT_NAME))
     huaweicloud_dew_block.update_secret_event(event_name = csms_params.get(EVENT_NAME), 
                                               target_type = csms_params.get("target_type"), 
                                               target_id = csms_params.get("target_id"), 
                                               target_name = csms_params.get("target_name"))
     huaweicloud_dew_block.delete_secret_event(event_name = csms_params.get(EVENT_NAME))
     huaweicloud_dew_block.list_notification_records()


@flow(name = 'test_key_label_management')
def test_key_label_management(kms_params):
     huaweicloud_dew_block.list_kms_tags()
     huaweicloud_dew_block.batch_create_kms_tags(key_id = kms_params.get(KEY_ID), key = [kms_params.get(KEY)], action = kms_params.get("action"))
     huaweicloud_dew_block.create_kms_tag(key_id = kms_params.get(KEY_ID), key = kms_params.get(KEY))
     huaweicloud_dew_block.list_kms_by_tags(DewBlock.ListKmsByTagsOptions(action = "count"))
     huaweicloud_dew_block.show_kms_tags(key_id = kms_params.get(KEY_ID))
     huaweicloud_dew_block.delete_tag(key_id = kms_params.get(KEY_ID), key = kms_params.get(KEY))

@flow(name = 'test_query_key_api_version_information')
def test_query_key_api_version_information(kms_params):
     huaweicloud_dew_block.show_versions()
     huaweicloud_dew_block.show_version(version_id = kms_params.get("version_id"))


if __name__ == "__main__":

    huaweicloud_dew_block = DewBlock.load("test-dew-block")