from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdkkms.v2.region.kms_region import KmsRegion
from huaweicloudsdkkms.v2 import KmsClient
from huaweicloudsdkkps.v3.region.kps_region import KpsRegion
from huaweicloudsdkkps.v3 import KpsClient
from huaweicloudsdkcsms.v1.region.csms_region import CsmsRegion
from huaweicloudsdkcsms.v1 import CsmsClient


class DewClient():
    def __init__(self,region_id = None, ak = None, sk = None, project_id = None, *args, **kwargs):
        if not ak or not sk:
                raise Exception("please input both huawei_cloud_access_key_id and huawei_cloud_secret_access_key")
        self.region_id = region_id
        self.__ak = ak
        self.__sk = sk
        self.__project_id=project_id
        self.kms_client = KmsClient.new_builder() \
            .with_credentials(BasicCredentials(self.__ak, self.__sk, self.__project_id)) \
            .with_region(KmsRegion.value_of(self.region_id)) \
            .build()
        self.kps_client = KpsClient.new_builder() \
            .with_credentials(BasicCredentials(self.__ak, self.__sk, self.__project_id)) \
            .with_region(KpsRegion.value_of(self.region_id)) \
            .build()
        self.cms_client = CsmsClient.new_builder() \
            .with_credentials(BasicCredentials(self.__ak, self.__sk, self.__project_id)) \
            .with_region(CsmsRegion.value_of(self.region_id)) \
            .build()
